
Description

Open-source, cross platform, sleep tracking software with a focus on monitoring CPAP treatment.

SleepyHead Web Site
Categories
Medical Science Apps.
License
GNU Library or Lesser General Public License version 3.0 (LGPLv3)
Features

    Free Cross Platform CPAP Review Software
    Fast, Interactive Mouse Controlled Graphs
    Supports Philips Respironics System One Machines
    Supports ResMed S9 CPAP machines
    Supports DeVilbiss Intellipap machines
    Supports Contec CMS50 Oximeters

